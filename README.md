# Yahoo finance API

we create a simple endpoint to allow access to the yahoo-finance api and get symbol prices

## The showing in console

port default : 8080
you can change it from env file

- [application base url](http://localhost:8080)

## yahoo-finance api name public

- [API to get prices of the given symbol list](http://localhost:8080/yahooFinanceApi?symbols=["AAPL","TSLA","MSFT","GOOG"]) [GET]

## How to use

make sure you create a get request with query params [symbols] contains the list of symbols desired.

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`PORT`=4005

## Running application

To run , run the following command
-to install all dependencies

```bash
  npm install
```

to run the server

```bash
  npm run start
```
