require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
app.use(express.json());
app.use(cors());
const yahooFinance = require("yahoo-finance");

/******************HTTP Client Request******************* */
app.get("/yahooFinanceApi", async (req, res) => {
  const query = req.query.symbols;
  const symbols = JSON.parse(query);
  const dateNow = new Date(Date.now().valueOf());
  const date52WeekAgo = new Date((Date.now() - 524160 * 60 * 1000).valueOf());
  const formattedDateNow = _formatDate(dateNow);
  const formattedDate52WeekAgo = _formatDate(date52WeekAgo);
  try {
    const history = await yahooFinance.historical({
      symbols,
      from: formattedDate52WeekAgo,
      to: formattedDateNow,
      period: "d",
    });
    const aapl = _calculateHighestLowestPrice(Object.values(history)[0]);
    const goog = _calculateHighestLowestPrice(Object.values(history)[1]);
    const tsla = _calculateHighestLowestPrice(Object.values(history)[2]);
    const msft = _calculateHighestLowestPrice(Object.values(history)[3]);
    return res.status(200).json([aapl, goog, tsla, msft]);
  } catch (error) {
    console.log({ error });
    return res.status(500).json({ error });
  }
});
/***************Format Date*************** */
const _formatDate = (date) => {
  return [
    date.getFullYear(),
    _padTo2Digits(date.getMonth() + 1),
    _padTo2Digits(date.getDate()),
  ].join("-");
};
const _padTo2Digits = (num) => {
  return num.toString().padStart(2, "0");
};
/***********Format Symbol Prices************** */
const _calculateHighestLowestPrice = (data) => {
  const dateNow = new Date(Date.now().valueOf() - 1440 * 60 * 1000);
  const formattedDateNow = _formatDate(dateNow);
  const weekly = getLowHigh(data);
  const daily = getLowHigh(
    data.filter((item) => _formatDate(item.date) === formattedDateNow)
  );
  return { id: data[0]["symbol"], daily, weekly };
};
const getLowHigh = (data) => {
  let price = {};
  for (let index = 0; index < data.length; index++) {
    const element = data[index];
    if (element.high > price.highest || !price.highest) {
      price.highest = element.high.toFixed(4);
    }
    if (element.low < price.lowest || !price.lowest) {
      price.lowest = element.low.toFixed(4);
    }
  }
  return price;
};
/*****************Server Listening ***************** */
app.listen(process.env.PORT, () => {
  console.log(`We are started on : http://localhost:${process.env.PORT}`);
  return `We are started on : http://localhost:${process.env.PORT}`;
});
